unit RepairToolMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, edbcomps, Vcl.StdCtrls,
  Vcl.Grids, Vcl.DBGrids, Vcl.ExtCtrls, Vcl.DBCtrls, Vcl.Samples.Spin,
  JvExDBGrids, JvDBGrid, Vcl.Menus, Vcl.ComCtrls;

type
  TForm1 = class(TForm)
    Database: TEDBDatabase;
    Session: TEDBSession;
    EDBEngine: TEDBEngine;
    TableNamesQuery: TEDBQuery;
    TableFieldQuery: TEDBQuery;
    TableNamesQueryName: TWideStringField;
    TableFieldQueryName: TWideStringField;
    MainTable: TEDBTable;
    dsMainTable: TDataSource;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    eDBN: TEdit;
    cbTableNames: TComboBox;
    cbFieldNames: TComboBox;
    Button1: TButton;
    Label3: TLabel;
    eLocate: TEdit;
    DBNavigator1: TDBNavigator;
    cbKeyFloat: TCheckBox;
    SpinMagnitude: TSpinEdit;
    Label4: TLabel;
    MainGrid: TJvDBGrid;
    PopupMenu1: TPopupMenu;
    NaNFinder1: TMenuItem;
    RepairNanDamage1: TMenuItem;
    cbSafeMode: TCheckBox;
    StatusBar1: TStatusBar;
    procedure Button1Click(Sender: TObject);
    procedure cbTableNamesChange(Sender: TObject);
    procedure eLocateKeyPress(Sender: TObject; var Key: Char);
    procedure MainGridDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure cbKeyFloatClick(Sender: TObject);
    procedure NaNFinder1Click(Sender: TObject);
    procedure RepairNanDamage1Click(Sender: TObject);
    procedure MainTableAfterScroll(DataSet: TDataSet);
  private
    FBusy :Boolean;

    { Private declarations }
    procedure GetTables;

    procedure GetFields;

    procedure LoadTable;

    procedure DoFilter;

    function FindGridColumn(colname:string):Integer;
    procedure MemoGetText(Sender: TField; var Text: string; DisplayText: Boolean);
    //TFieldGetTextEvent = procedure(Sender: TField; var Text: string;
    //DisplayText: Boolean) of object;

    procedure DoKeyFloatField(hideOther:Boolean);
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

uses
  Math;

procedure TForm1.Button1Click(Sender: TObject);
begin
   TableNamesQuery.Active := false;

   TableFieldQuery.Active := false;

   Session.Connected := false;
   Database.Connected := false;
   Database.Database := eDBN.Text;
   Session.Connected := true;
   Database.Connected := true;


   cbFieldNames.Enabled := true;
   cbTableNames.Enabled := true;

  GetTables;

  if cbTableNames.Items.IndexOf('Transaction File')>0 then
      cbTableNames.ItemIndex := cbTableNames.Items.IndexOf('Transaction File');

  GetFields;

  if cbFieldNames.Items.IndexOf('Unique ID')>0 then
      cbFieldNames.ItemIndex := cbFieldNames.Items.IndexOf('Unique ID');

  LoadTable;


end;

procedure TForm1.cbKeyFloatClick(Sender: TObject);
begin
  DoKeyFloatField(cbKeyFloat.Checked);
end;

procedure TForm1.cbTableNamesChange(Sender: TObject);
begin
  GetFields;
end;

(*
TFieldType = (ftUnknown, ftString, ftSmallint, ftInteger, ftWord, // 0..4
    ftBoolean, ftFloat, ftCurrency, ftBCD, ftDate, ftTime, ftDateTime, // 5..11
    ftBytes, ftVarBytes, ftAutoInc, ftBlob, ftMemo, ftGraphic, ftFmtMemo, // 12..18
    ftParadoxOle, ftDBaseOle, ftTypedBinary, ftCursor, ftFixedChar, ftWideString, // 19..24
    ftLargeint, ftADT, ftArray, ftReference, ftDataSet, ftOraBlob, ftOraClob, // 25..31
    ftVariant, ftInterface, ftIDispatch, ftGuid, ftTimeStamp, ftFMTBcd, // 32..37
    ftFixedWideChar, ftWideMemo, ftOraTimeStamp, ftOraInterval, // 38..41
    ftLongWord, ftShortint, ftByte, ftExtended, ftConnection, ftParams, ftStream, //42..48
    ftTimeStampOffset, ftObject, ftSingle); //49..51
*)
function StringlyTyped (fieldType:TFieldType):Boolean;
begin
  result := fieldType in [ftString,ftWideString,ftMemo,ftFixedChar,ftFixedWideChar,ftWideMemo];
end;

function NumTyped (fieldType:TFieldType):Boolean;
begin
  result := fieldType in [ftAutoInc,ftSmallInt,ftINteger,ftWord,ftFloat,ftCurrency,ftBCD,ftLargeInt,ftExtended];
end;

function FloatyTyped (fieldType:TFieldType):Boolean;
begin
  result := fieldType in [ftFloat,ftCurrency,ftExtended ];
end;

function DateTyped(fieldType:TFieldType):Boolean;
begin
  result := fieldType in [ftDate,ftDateTime];
end;

function FieldMemoTyped(fieldType:TFieldType):Boolean;
begin
  result := fieldType in [ftMemo,ftWideMemo];
end;


procedure TForm1.MainGridDrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
 n:Integer;
 Field:TField;
 FlagCount:Integer;
 BS_Saved:TBrushStyle;
 FlagCol:Boolean;
 r2:TRect;
begin
 FlagCount:= 0;
 FlagCol := false;
 try
  for n := 0 to MainTable.FieldCount - 1 do
  begin
    Field := MainTable.Fields[n];
    if Assigned(Field) and FloatyTyped(Field.DataType) then
    begin
      if Abs(Field.AsFloat)> SpinMagnitude.Value then
      begin
        Inc(FlagCount);
        if Column.Field=Field then
            FlagCol := true;
      end;
    end;
  end;
 except
    FlagCount := FlagCount+ 10000;
 end;
 if FlagCount>0 then
 begin
   if FlagCount<3 then
      MainGrid.Canvas.Pen.Color := clYellow
   else
      MainGrid.Canvas.Pen.Color := clRed;

   if FlagCol then
   begin

       r2 := Rect;
       r2.Right := r2.Left+2;
       r2.Bottom := r2.Top+2;
       MainGrid.Canvas.Brush.Color := clYellow;
       MainGrid.Canvas.Rectangle(r2);

   end;


    BS_Saved := MainGrid.Canvas.Brush.Style;
    MainGrid.Canvas.Brush.Style := bsClear;
    MainGrid.Canvas.Rectangle(Rect);
    MainGrid.Canvas.Brush.Style := BS_Saved;
 end
 else
 begin
    MainGrid.Canvas.Brush.Style := bsSolid;
    MainGrid.Canvas.Brush.Color := clWindow;
    MainGrid.Canvas.Pen.Color := clBlack;
    MainGrid.Canvas.Font.Color := clWindowText;
 end;


end;

procedure TForm1.MainTableAfterScroll(DataSet: TDataSet);
begin
 try
  if not FBusy then
  begin
      StatusBar1.Panels[0].Text := 'Row '+IntToStr(DataSet.RecNo) +' / '+IntToStr(DataSet.REcordCount);
  end;
 except
 end;
end;

procedure TForm1.DoFilter;
var
 selectedField:TField;

 function GetFieldNameQuote:string;
 begin
    result := '"'+cbFieldNames.Text+'"';

 end;
begin
  if not MainTable.Active then exit;

  if eLocate.Text = '' then
  begin
    MainTable.Filtered := false;
    MainTable.Filter := '';
    exit;
  end;

  selectedField := MainTable.FieldByName(cbFieldNames.Text);

  if StringlyTyped(selectedField.DataType) then
  begin

    MainTable.Filter := GetFieldNameQuote +' like '''+eLocate.Text+'''%';
    MainTable.Filtered := true;
  end else
  if NumTyped(selectedField.DataType) then
  begin
    if SameText(eLocate.Text,'NAN') then
    begin
    MainTable.Filter := GetFieldNameQuote +' = NaN';
    MainTable.Filtered := true;

    end
    else
    begin
     if eLocate.Text[1]='>' then
      MainTable.Filter := GetFieldNameQuote +' > '+Copy(eLocate.Text,2,100)
    else if eLocate.Text[1]='<' then
      MainTable.Filter := GetFieldNameQuote +' < '+Copy(eLocate.Text,2,100)
    else
     MainTable.Filter := GetFieldNameQuote +' = '+IntToStr(StrToIntDef( eLocate.Text, -1 ));

    MainTable.Filtered := true;
    end;
  end
  else if DateTyped(selectedField.DataType) then
  begin


    if eLocate.Text[1]='>' then
      MainTable.Filter := GetFieldNameQuote +' > date '''+Copy(eLocate.Text,2,100)+''''
    else if eLocate.Text[1]='<' then
      MainTable.Filter := GetFieldNameQuote +' < date '''+Copy(eLocate.Text,2,100)+''''
    else
      MainTable.Filter := GetFieldNameQuote +' = date '''+eLocate.Text+'''';

    MainTable.Filtered := true;


  end;



end;

procedure TForm1.DoKeyFloatField(hideOther: Boolean);
var
 n:Integer;
begin

  for n := 0 to MainGrid.Columns.Count-1 do
  begin

  MainGrid.Columns[n].Visible := (not hideOther) or (  (n<3)or( FloatyTyped(MainGrid.Columns[n].Field.DataType )) );
  end;

end;

procedure TForm1.eLocateKeyPress(Sender: TObject; var Key: Char);
begin
   if Key=#13 then
   begin
    try
      DoFilter;
    except
      on E:Exception do
      begin
        ShowMessage('Unable to filter with this expression. '+MainTable.Filter+' '#13#10+  E.Message);
        eLocate.Text := '';
      end;
    end;
   end;
end;

function TForm1.FindGridColumn(colname: string): Integer;
var
 t:Integer;
begin
  result := -1;

  for t := 0 to MainGrid.Columns.Count-1 do
  begin
    if SameText(MainGrid.Columns[t].FieldName,colname) then
    begin
      result := t;
      exit;
    end;
  end;
end;

procedure TForm1.GetFields;
begin
   cbFieldNames.Items.Clear;
   TableFieldQuery.Active := false;

   TableFieldQuery.Params[0].Value := cbTableNames.Text;
   TableFieldQuery.Active := true;

   while not TableFieldQuery.Eof do
   begin
     cbFieldNames.Items.Add( TableFieldQueryName .Value );
     TableFieldQuery.Next;
   end;
   TableFieldQuery.Active := true;

   cbFieldNames.ItemIndex := 0;

end;

procedure TForm1.GetTables;
begin
   cbTableNames.Items.Clear;
   TableNamesQuery.Active := false;
   TableNamesQuery.Active := true;
   while not TableNamesQuery.Eof do
   begin
     cbTableNames.Items.Add( TableNamesQueryName.Value );
     TableNamesQuery.Next;
   end;
   TableNamesQuery.Active := true;

   cbTableNames.ItemIndex := 0;
end;

procedure TForm1.LoadTable;
var
  idx:Integer;
  n:Integer;
begin

  MainTable.Active := false;
  MainTable.TableName := cbTableNames.Text;
  MainTable.Active := true;

  MainGrid.Visible := true;

  // if "Unique ID" exists, make it the first column:

  idx := FindGridColumn('Unique ID');
  if (idx>0) then
      MainGrid.Columns[idx].Index := 0;
  idx := FindGridColumn('Date');
  if (idx>0) then
      MainGrid.Columns[idx].Index := 1;
  idx := FindGridColumn('Time');
  if (idx>0) then
      MainGrid.Columns[idx].Index := 1;
  idx := FindGridColumn('Transaction Type');
  if (idx>0) then
      MainGrid.Columns[idx].Index := 2;
  idx := FindGridColumn('Item Number');
  if (idx>0) then
      MainGrid.Columns[idx].Index := 3;

  for n := 0 to MainGrid.Columns.Count-1 do
  begin
    if FieldMemoTyped(MainGrid.Columns[n].Field.DataType) then
    begin
      MainGrid.Columns[n].Field.OnGetText := MemoGetText;

    end;

  end;
end;

procedure TForm1.MemoGetText(Sender: TField; var Text: string;
  DisplayText: Boolean);
begin
  DisplayText := True;
  Text := Copy(Sender.AsString,1,80);
end;

procedure TForm1.NaNFinder1Click(Sender: TObject);
var
  n:Integer;
  Field:TField;
  Found:Boolean;
  RowCounter:Integer;
begin
  Screen.Cursor := crHourglass;
  FBusy := true;

  Found := false;
  RowCounter := 0;
  try
    MainGrid.DataSource := nil;
    MainGrid.Visible :=false;
    StatusBar1.Panels[0].Text := 'Searching for NaN values... Please wait.';


    MainTable.Last;

    while (not Found) and (not MainTable.Bof) do
    begin
        Inc(RowCounter);
        if cbSafeMode.Checked and ( (RowCounter mod 10000) = 0 ) then
        begin
            OutputDebugString(PChar('Row Count '+IntToStr(RowCounter)));
            Application.ProcessMessages;
            Sleep(1000);
            Application.ProcessMessages;
            Sleep(1000);
            Application.ProcessMessages;
        end;

        for n := 0 to MainTable.FieldCount-1 do
        begin
          Field := MainTable.Fields[n];
          if FloatyTyped(Field.DataType) then
          begin
              if IsNan(Field.AsFloat) then
              begin
                  OutputDebugString('Found');
                  Found := true;
                  break;
              end;
          end;


        end;

        MainTable.Prior;

    end;

  finally
    MainGrid.DataSource := dsMainTable;
    MainGrid.Visible :=true;
    Screen.Cursor := crDefault;
    FBusy := false;
    StatusBar1.Panels[0].Text := 'Complete.';
  end;
  OutputDebugString(PChar('Row Count '+IntToStr(RowCounter)));

  if not Found then
       ShowMessage('No NaN found')
  else
     ShowMessage('Nan found');
end;

procedure TForm1.RepairNanDamage1Click(Sender: TObject);
var
  n:Integer;
  Field:TField;
  Found:Integer;
  RowCounter:Integer;
  Fail :Integer;
begin
  Screen.Cursor := crHourglass;
  FBusy := true;
  Fail := 0;
  RowCounter := 0;

  Found := 0;
  try
    MainGrid.DataSource := nil;
    MainGrid.Visible :=false;
    StatusBar1.Panels[0].Text := 'Repairing NaN Damage... Please wait.';

    MainTable.Last;

    while (not MainTable.Bof) do
    begin
        Inc(RowCounter);

        if cbSafeMode.Checked and ( (RowCounter mod 10000)=0 ) then
        begin
           OutputDebugString(PChar('Row Count '+IntToStr(RowCounter)));

            Application.ProcessMessages;
            Sleep(500);
            Application.ProcessMessages;
            Sleep(500);
            Application.ProcessMessages;
        end;

        for n := 0 to MainTable.FieldCount-1 do
        begin
          Field := MainTable.Fields[n];
          if FloatyTyped(Field.DataType) then
          begin
              if IsNan(Field.AsFloat) then
              begin

                 try
                  Inc(Found);
                  MainTable.Edit;
                  Field.Value := 0;
                  MainTable.Post;
                 except
                  on E:Exception do
                  begin
                    OutputDebugString('Lock or other problem. Skipping repair.');
                    OutputDebugString(PChar(e.message));
                    Inc(Fail);
                  end;
                 end;

              end;
          end;


        end;

        MainTable.Prior;

    end;

  finally
    MainGrid.DataSource := dsMainTable;
    MainGrid.Visible := true;
    Screen.Cursor := crDefault;
    FBusy := false;
    if (Fail>0) then
        StatusBar1.Panels[0].Text := 'Complete. Skipped '+IntToStr(Fail)+' due to locks or other problems '
    else
      StatusBar1.Panels[0].Text := 'Complete. ';



  end;
  OutputDebugString(PChar('Row Count '+IntToStr(RowCounter)));

  if  Found =0 then
       ShowMessage('No NaN found')
  else
     ShowMessage( IntToStr(Found)+' Nan values found and removed ');
end;

end.
