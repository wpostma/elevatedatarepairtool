unit ExportToolMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, edbcomps, Vcl.StdCtrls,
  Vcl.Grids, Vcl.DBGrids, Vcl.ExtCtrls, Vcl.DBCtrls, Vcl.Samples.Spin,
  JvExDBGrids, JvDBGrid, Vcl.Menus, Vcl.ComCtrls, JvCsvData, JvDBGridExport,
  JvComponentBase;

const
  MainTableName = 'MercuryTransLog2';
  MainTableName2 = 'MercuryTransLog';

type
  TForm1 = class(TForm)
    Database: TEDBDatabase;
    Session: TEDBSession;
    EDBEngine: TEDBEngine;
    TableNamesQuery: TEDBQuery;
    TableFieldQuery: TEDBQuery;
    TableNamesQueryName: TWideStringField;
    TableFieldQueryName: TWideStringField;
    MainTable: TEDBTable;
    dsMainTable: TDataSource;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    eDBN: TEdit;
    cbTableNames: TComboBox;
    cbFieldNames: TComboBox;
    Button1: TButton;
    Label3: TLabel;
    eLocate: TEdit;
    DBNavigator1: TDBNavigator;
    cbKeyFloat: TCheckBox;
    SpinMagnitude: TSpinEdit;
    Label4: TLabel;
    MainGrid: TJvDBGrid;
    PopupMenu1: TPopupMenu;
    StatusBar1: TStatusBar;
    ExportData1: TMenuItem;
    ExportAllByMonth1: TMenuItem;
    JvCsvDataSet1: TJvCsvDataSet;
    DBMemoCLOB: TDBMemo;
    Splitter1: TSplitter;
    MainTableTransactionID: TAutoIncField;
    MainTableTransRequet: TWideMemoField;
    MainTableTransResponse: TWideMemoField;
    MainTableAmount: TFloatField;
    MainTableAuthorizationCode: TWideStringField;
    MainTableApproved: TBooleanField;
    MainTableReferenceNumber: TWideStringField;
    MainTableInvoiceNumber: TWideStringField;
    MainTableProductName: TWideStringField;
    MainTableProductReceiptNumber: TWideStringField;
    MainTableCMDStatus: TWideStringField;
    MainTableTextResponse: TWideStringField;
    MainTableSequenceNumber: TWideStringField;
    MainTableEmployeeID: TIntegerField;
    MainTableStationID: TIntegerField;
    MainTableCurrentDate: TDateField;
    MainTableCurrentTime: TTimeField;
    MainTableClientReceipt: TWideMemoField;
    MainTableMerchantReceipt: TWideMemoField;
    MainTableUpdateCentralFlag: TIntegerField;
    MainTableUpdateCentralDate: TDateField;
    MainTableUpdateCentralTime: TTimeField;
    MainTableCardType: TStringField;
    MainTableCardApprovalInfo: TStringField;
    Button2: TButton;
    XlsExporter: TJvDBGridExcelExport;
    csvExporter: TJvDBGridCSVExport;
    procedure Button1Click(Sender: TObject);
    procedure cbTableNamesChange(Sender: TObject);
    procedure eLocateKeyPress(Sender: TObject; var Key: Char);
    procedure MainGridDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure cbKeyFloatClick(Sender: TObject);

    procedure MainTableAfterScroll(DataSet: TDataSet);
    procedure MainTableCardTypeGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure MainTableCardApprovalInfoGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure Button2Click(Sender: TObject);
    procedure MainTableCalcFields(DataSet: TDataSet);
    procedure eLocateExit(Sender: TObject);
  private
    FBusy :Boolean;

    function CardTypeGetText: String;
    function CardApprovalGetText: String;


    { Private declarations }
    procedure GetTables;

    procedure GetFields;

    procedure LoadTable;

    procedure DoFilter;

    function FindGridColumn(colname:string):Integer;
    procedure MemoGetText(Sender: TField; var Text: string; DisplayText: Boolean);
    //TFieldGetTextEvent = procedure(Sender: TField; var Text: string;
    //DisplayText: Boolean) of object;

    procedure DoKeyFloatField(hideOther:Boolean);
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation


{$R *.dfm}

uses
  StrUtils,
  Math;

procedure TForm1.Button1Click(Sender: TObject);
begin
   TableNamesQuery.Active := false;

   TableFieldQuery.Active := false;

   Session.Connected := false;
   Database.Connected := false;
   Database.Database := eDBN.Text;
   Session.Connected := true;
   Database.Connected := true;


   cbFieldNames.Enabled := true;
   cbTableNames.Enabled := true;

  GetTables;

  if cbTableNames.Items.IndexOf(MainTableName)>0 then
      cbTableNames.ItemIndex := cbTableNames.Items.IndexOf(MainTableName)
  else
   if cbTableNames.Items.IndexOf(MainTableName2)>0 then
       cbTableNames.ItemIndex := cbTableNames.Items.IndexOf(MainTableName2);


  GetFields;

  if cbFieldNames.Items.IndexOf('CurrentDate')>0 then
      cbFieldNames.ItemIndex := cbFieldNames.Items.IndexOf('CurrentDate');

  LoadTable;


end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  if not MainTable.Active then
  begin
    ShowMessage('Get connected first.');

    exit;
  end;

  Screen.Cursor := crHourglass;
  try
    try
    if eLocate.Text<>'' then
     DoFilter;
    csvExporter.FileName := cbTableNames.Text+'_'+eLocate.Text+'.csv';
    if csvExporter.ExportGrid then
        ShowMessage('Exported '+csvExporter.FileName)
    else
        ShowMessage('Export failed');


  except
    on E:Exception do
    begin
      ShowMessage('Filter or Export Error '+E.Message);
    end;
  end;
  finally
    Screen.Cursor := crDefault;
  end;

end;

procedure TForm1.cbKeyFloatClick(Sender: TObject);
begin
  DoKeyFloatField(cbKeyFloat.Checked);
end;

procedure TForm1.cbTableNamesChange(Sender: TObject);
begin
  GetFields;
end;

(*
TFieldType = (ftUnknown, ftString, ftSmallint, ftInteger, ftWord, // 0..4
    ftBoolean, ftFloat, ftCurrency, ftBCD, ftDate, ftTime, ftDateTime, // 5..11
    ftBytes, ftVarBytes, ftAutoInc, ftBlob, ftMemo, ftGraphic, ftFmtMemo, // 12..18
    ftParadoxOle, ftDBaseOle, ftTypedBinary, ftCursor, ftFixedChar, ftWideString, // 19..24
    ftLargeint, ftADT, ftArray, ftReference, ftDataSet, ftOraBlob, ftOraClob, // 25..31
    ftVariant, ftInterface, ftIDispatch, ftGuid, ftTimeStamp, ftFMTBcd, // 32..37
    ftFixedWideChar, ftWideMemo, ftOraTimeStamp, ftOraInterval, // 38..41
    ftLongWord, ftShortint, ftByte, ftExtended, ftConnection, ftParams, ftStream, //42..48
    ftTimeStampOffset, ftObject, ftSingle); //49..51
*)
function StringlyTyped (fieldType:TFieldType):Boolean;
begin
  result := fieldType in [ftString,ftWideString,ftMemo,ftFixedChar,ftFixedWideChar,ftWideMemo];
end;

function NumTyped (fieldType:TFieldType):Boolean;
begin
  result := fieldType in [ftAutoInc,ftSmallInt,ftINteger,ftWord,ftFloat,ftCurrency,ftBCD,ftLargeInt,ftExtended];
end;

function FloatyTyped (fieldType:TFieldType):Boolean;
begin
  result := fieldType in [ftFloat,ftCurrency,ftExtended ];
end;

function DateTyped(fieldType:TFieldType):Boolean;
begin
  result := fieldType in [ftDate,ftDateTime];
end;

function FieldMemoTyped(fieldType:TFieldType):Boolean;
begin
  result := fieldType in [ftMemo,ftWideMemo];
end;

function BoolTyped(fieldType:TFieldType):Boolean;
begin
  result := fieldType in [ftBoolean];
end;



procedure TForm1.MainGridDrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
 n:Integer;
 Field:TField;
 FlagCount:Integer;
 BS_Saved:TBrushStyle;
 FlagCol:Boolean;
 r2:TRect;
begin
 FlagCount:= 0;
 FlagCol := false;
 try
  for n := 0 to MainTable.FieldCount - 1 do
  begin
    Field := MainTable.Fields[n];
    if Assigned(Field) and FloatyTyped(Field.DataType) then
    begin
      if Abs(Field.AsFloat)> SpinMagnitude.Value then
      begin
        Inc(FlagCount);
        if Column.Field=Field then
            FlagCol := true;
      end;
    end;
  end;
 except
    FlagCount := FlagCount+ 10000;
 end;
 if FlagCount>0 then
 begin
   if FlagCount<3 then
      MainGrid.Canvas.Pen.Color := clYellow
   else
      MainGrid.Canvas.Pen.Color := clRed;

   if FlagCol then
   begin

       r2 := Rect;
       r2.Right := r2.Left+2;
       r2.Bottom := r2.Top+2;
       MainGrid.Canvas.Brush.Color := clYellow;
       MainGrid.Canvas.Rectangle(r2);

   end;


    BS_Saved := MainGrid.Canvas.Brush.Style;
    MainGrid.Canvas.Brush.Style := bsClear;
    MainGrid.Canvas.Rectangle(Rect);
    MainGrid.Canvas.Brush.Style := BS_Saved;
 end
 else
 begin
    MainGrid.Canvas.Brush.Style := bsSolid;
    MainGrid.Canvas.Brush.Color := clWindow;
    MainGrid.Canvas.Pen.Color := clBlack;
    MainGrid.Canvas.Font.Color := clWindowText;
 end;


end;

procedure TForm1.MainTableAfterScroll(DataSet: TDataSet);
begin
 try
  if not FBusy then
  begin
      StatusBar1.Panels[0].Text := 'Row '+IntToStr(DataSet.RecNo) +' / '+IntToStr(DataSet.REcordCount);
  end;
 except
 end;
end;

   {
	<PrintData>
		<Line1>.MERCHANT ID: 4532058E</Line1>
		<Line2>.</Line2>
		<Line3>.                  SALE                  </Line3>
		<Line4>.</Line4>
		<Line5>.************2062</Line5>
		<Line6>.DEBIT                ENTRY METHOD: CHIP</Line6>
		<Line7>.ACCT TYPE: CHEQUING</Line7>
		<Line8>.</Line8>
		<Line9>.DATE: 2017/03/19  TIME: 10:34:47</Line9>
		<Line10>.</Line10>
		<Line11>.INV#: 3             APPR CODE: 133447   </Line11>
		<Line12>.SEQ #: 0010010500</Line12>
		<Line13>.RETRIEVAL #: 0002</Line13>
		<Line14>.</Line14>
		<Line15>.AMOUNT                      CAD$ 18.90</Line15>
		<Line16>.TIP                          CAD$ 0.00</Line16>
		<Line17>.                            ==========</Line17>
		<Line18>.TOTAL                       CAD$ 18.90</Line18>
		<Line19>.</Line19>
		<Line20>.    00    APPROVED - THANK YOU    001   </Line20>
		<Line21>.</Line21>
		<Line22>.</Line22>
		<Line23>.Application Label:</Line23>
		<Line24>.Interac</Line24>
		<Line25>.AID:A0000002771010</Line25>  <-- these numbers change. Not always line 25!!!!
		<Line26>.TVR:8080008000</Line26>
		<Line27>.TSI:7800</Line27>
		<Line28>.RESP CD:00</Line28>
	</PrintData>
  }
procedure TForm1.MainTableCalcFields(DataSet: TDataSet);
begin

  MainTableCardType.AsString :=    CardTypeGetText;
  MainTableCardApprovalInfo.AsString := CardApprovalGetText;
end;

procedure TForm1.MainTableCardApprovalInfoGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
 Text := CardApprovalGetText;
end;


function TForm1.CardApprovalGetText: String;
var
  n1,n2:Integer;
  s:string;
  tmp:string;
begin

  s := MainTableTransResponse.AsString;
  tmp := '';

  n1 := Pos('ACCT TYPE: ',s);
  n2 := 0;
  if (n1>0) then
     n2 := Pos('</',s,n1);

  if (n1>0) and (n2>0) then
  begin
      tmp := StringReplace( Copy(s, n1, n2-n1), '  ',' ',[rfReplaceAll] );
  end;

  n1 := Pos('AID:',s);
  n2 := 0;
  if (n1>0) then
    n2 := PosEx('</',s,n1);
  if (n1>0) and (n2>0) then
  begin
      tmp := tmp + ' '+StringReplace( Copy(s, n1, n2-n1), '  ',' ',[rfReplaceAll] );
  end;


  n1 := Pos('TVR:',s);
  n2 := 0;
  if (n1>0) then
    n2 := Pos('</',s,n1);

  if (n1>0) and (n2>0) then
  begin
      tmp := tmp + ' '+StringReplace( Copy(s, n1, n2-n1), '  ',' ',[rfReplaceAll] );
  end;

  n1 := Pos('TSI:',s);
  n2 := 0;
  if (n1>0) then
    n2 := Pos('</',s,n1);

  if (n1>0) and (n2>0) then
  begin
      tmp := tmp + ' '+StringReplace( Copy(s, n1, n2-n1), '  ',' ',[rfReplaceAll] );
  end;



  if tmp='' then
      tmp := 'n/a ('+MainTableCMDStatus.AsString+')';



  Result := tmp;

end;

procedure TForm1.MainTableCardTypeGetText(Sender: TField; var Text: string;
  DisplayText: Boolean);
begin
  Text := CardTypeGetText;
end;

function TForm1.CardTypeGetText:string;
var
  n1,n2:Integer;
  s:string;
  tmp:string;
begin
  s := MainTableTransResponse.AsString;
  n1 := Pos('<Line6>.',s);
  n2 := Pos('</Line6>',s);
  if (n1>0) and (n2>0) then
  begin
      n1 := n1 + 8;
      tmp := StringReplace( Copy(s, n1, n2-n1), '  ',' ',[rfReplaceAll] );
      tmp := StringReplace( tmp, '  ',' ',[rfReplaceAll] );

  end;

  n1 := Pos('<Line5>.',s);
  n2 := Pos('</Line5>',s);
  if (n1>0) and (n2>0) then
  begin
      n1 := n1 + 8;
      tmp  := tmp + ' ' +StringReplace( Copy(s, n1, n2-n1), '  ',' ',[rfReplaceAll] );

  end;


  Result := tmp;
end;

function IsoDate(adate:TDate):string;
begin
  result := FormatDateTime('yyyy-mm-dd',adate);
end;

procedure TForm1.DoFilter;
var
 startDate,endDate:TDate;
 startDateStr:string;
 selectedField:TField;
 StartDateY,StartDateM,StartDateD: word;

 function GetFieldNameQuote:string;
 begin
    result := '"'+cbFieldNames.Text+'"';

 end;
begin
  if not MainTable.Active then exit;

  if eLocate.Text = '' then
  begin
    MainTable.Filtered := false;
    MainTable.Filter := '';
    exit;
  end;

  selectedField := MainTable.FieldByName(cbFieldNames.Text);

  if StringlyTyped(selectedField.DataType) then
  begin

    MainTable.Filter := GetFieldNameQuote +' like '''+eLocate.Text+'''%';
    MainTable.Filtered := true;
  end else
  if NumTyped(selectedField.DataType) then
  begin
    if SameText(eLocate.Text,'NAN') then
    begin
    MainTable.Filter := GetFieldNameQuote +' = NaN';
    MainTable.Filtered := true;

    end
    else
    begin
     if eLocate.Text[1]='>' then
      MainTable.Filter := GetFieldNameQuote +' > '+Copy(eLocate.Text,2,100)
    else if eLocate.Text[1]='<' then
      MainTable.Filter := GetFieldNameQuote +' < '+Copy(eLocate.Text,2,100)
    else
     MainTable.Filter := GetFieldNameQuote +' = '+IntToStr(StrToIntDef( eLocate.Text, -1 ));

    MainTable.Filtered := true;
    end;
  end
  else if BoolTyped(selectedField.DataType) then
  begin
    if (eLocate.Text='1') or (SameText(eLocate.Text,'true'))
    then
      MainTable.Filter := GetFieldNameQuote +' = true'
    else
      MainTable.Filter := GetFieldNameQuote +' = false';

    MainTable.Filtered := true;

  end
  else if DateTyped(selectedField.DataType) then
  begin

    if (Pos('*', eLocate.Text)>0)or (Length(eLocate.Text)=7) then
    begin
        if (Length(eLocate.Text)=7) then
              startDateStr := eLocate.Text+'-01'
        else
         startDateStr := StringReplace(eLocate.Text,'*','01',[rfReplaceAll]);

      startDate := StrToDate(startDateStr);
      DecodeDate(startDate, StartDateY, StartDateM, StartDateD);
      Inc(StartDateM);
      if (StartDateM>12) then
      begin
        StartDateM := 1;
        Inc(StartDateY);
      end;
      endDate := EncodeDate(StartDateY,StartDateM,StartDateD);

      MainTable.Filter := GetFieldNameQuote +' >= date '''+IsoDate(startDate)+''' and '+
          GetFieldNameQuote +' < date '''+IsoDate(endDate) +'''';





    end
    else
    begin

    if eLocate.Text[1]='>' then
      MainTable.Filter := GetFieldNameQuote +' > date '''+Copy(eLocate.Text,2,100)+''''
    else if eLocate.Text[1]='<' then
      MainTable.Filter := GetFieldNameQuote +' < date '''+Copy(eLocate.Text,2,100)+''''
    else
      MainTable.Filter := GetFieldNameQuote +' = date '''+eLocate.Text+'''';

    end;

    MainTable.Filtered := true;


  end;



end;

procedure TForm1.DoKeyFloatField(hideOther: Boolean);
var
 n:Integer;
begin

  for n := 0 to MainGrid.Columns.Count-1 do
  begin

  MainGrid.Columns[n].Visible := (not hideOther) or (  (n<3)or( FloatyTyped(MainGrid.Columns[n].Field.DataType )) );
  end;

end;

procedure TForm1.eLocateExit(Sender: TObject);
begin
    try
      DoFilter;
    except
      on E:Exception do
      begin
        //ShowMessage('Unable to filter with this expression. '+MainTable.Filter+' '#13#10+  E.Message);
        eLocate.Text := '';
      end;
    end;

end;

procedure TForm1.eLocateKeyPress(Sender: TObject; var Key: Char);
begin
   if Key=#13 then
   begin
    try
      DoFilter;
    except
      on E:Exception do
      begin
        ShowMessage('Unable to filter with this expression. '+MainTable.Filter+' '#13#10+  E.Message);
        eLocate.Text := '';
      end;
    end;
   end;
end;

function TForm1.FindGridColumn(colname: string): Integer;
var
 t:Integer;
begin
  result := -1;

  for t := 0 to MainGrid.Columns.Count-1 do
  begin
    if SameText(MainGrid.Columns[t].FieldName,colname) then
    begin
      result := t;
      exit;
    end;
  end;
end;

procedure TForm1.GetFields;
begin
   cbFieldNames.Items.Clear;
   TableFieldQuery.Active := false;

   TableFieldQuery.Params[0].Value := cbTableNames.Text;
   TableFieldQuery.Active := true;

   while not TableFieldQuery.Eof do
   begin
     cbFieldNames.Items.Add( TableFieldQueryName .Value );
     TableFieldQuery.Next;
   end;
   TableFieldQuery.Active := true;

   cbFieldNames.ItemIndex := 0;

end;

procedure TForm1.GetTables;
begin
   cbTableNames.Items.Clear;
   TableNamesQuery.Active := false;
   TableNamesQuery.Active := true;
   while not TableNamesQuery.Eof do
   begin
     cbTableNames.Items.Add( TableNamesQueryName.Value );
     TableNamesQuery.Next;
   end;
   TableNamesQuery.Active := true;

   cbTableNames.ItemIndex := 0;
end;

procedure TForm1.LoadTable;
var
  idx:Integer;
  n:Integer;
begin

  MainTable.Active := false;
  MainTable.TableName := cbTableNames.Text;
  MainTable.Active := true;

  MainGrid.Visible := true;

  // if "Unique ID" exists, make it the first column:

  idx := FindGridColumn('CurrentDate');
  if (idx>0) then
      MainGrid.Columns[idx].Index := 1;
  idx := FindGridColumn('CurrentTime');
  if (idx>0) then
      MainGrid.Columns[idx].Index := 2;

  idx := FindGridColumn('CardType');
  if (idx>0) then
      MainGrid.Columns[idx].Index := 8;

  idx := FindGridColumn('CardApprovalInfo');
  if (idx>0) then
      MainGrid.Columns[idx].Index := 9;

  idx := FindGridColumn('CMDStatus');
  if (idx>0) then
      MainGrid.Columns[idx].Index := 10;

  idx := FindGridColumn('TextResponse');
  if (idx>0) then
      MainGrid.Columns[idx].Index := 11;



  for n := 0 to MainGrid.Columns.Count-1 do
  begin
    if FieldMemoTyped(MainGrid.Columns[n].Field.DataType) then
    begin
      IF NOT Assigned(MainGrid.Columns[n].Field.OnGetText) THEN
        MainGrid.Columns[n].Field.OnGetText := MemoGetText;

    end;

  end;
end;

procedure TForm1.MemoGetText(Sender: TField; var Text: string;
  DisplayText: Boolean);
begin
  DisplayText := True;
  Text := Copy(Sender.AsString,1,32000);
  if Text<>'' then
  begin
    Text := StringReplace(Text,#13#10,#13,[rfReplaceAll] );


  end;
end;



end.
