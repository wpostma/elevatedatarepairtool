object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Elevate Table Export Tool'
  ClientHeight = 411
  ClientWidth = 825
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 631
    Top = 105
    Width = 9
    Height = 287
    Align = alRight
    Color = clGray
    ParentColor = False
    ExplicitLeft = 447
    ExplicitTop = 81
    ExplicitHeight = 230
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 825
    Height = 105
    Align = alTop
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 16
      Width = 20
      Height = 13
      Caption = 'DBN'
    end
    object Label2: TLabel
      Left = 168
      Top = 16
      Width = 17
      Height = 13
      Caption = 'TBL'
    end
    object Label3: TLabel
      Left = 341
      Top = 16
      Width = 28
      Height = 13
      Caption = 'FIELD'
    end
    object Label4: TLabel
      Left = 508
      Top = 40
      Width = 92
      Height = 13
      Caption = 'Large Value Flag at'
    end
    object eDBN: TEdit
      Left = 34
      Top = 13
      Width = 63
      Height = 21
      TabOrder = 0
      Text = 'Dummy'
    end
    object cbTableNames: TComboBox
      Left = 190
      Top = 13
      Width = 145
      Height = 21
      Style = csDropDownList
      Enabled = False
      ItemIndex = 0
      TabOrder = 1
      Text = 'Transaction File'
      OnChange = cbTableNamesChange
      Items.Strings = (
        'Transaction File'
        'Master'
        'Detail')
    end
    object cbFieldNames: TComboBox
      Left = 374
      Top = 13
      Width = 145
      Height = 21
      Style = csDropDownList
      Enabled = False
      TabOrder = 2
    end
    object Button1: TButton
      Left = 103
      Top = 11
      Width = 50
      Height = 25
      Caption = 'connect'
      TabOrder = 3
      OnClick = Button1Click
    end
    object eLocate: TEdit
      Left = 525
      Top = 13
      Width = 89
      Height = 21
      TabOrder = 4
      TextHint = 'Search for...'
      OnExit = eLocateExit
      OnKeyPress = eLocateKeyPress
    end
    object DBNavigator1: TDBNavigator
      Left = 8
      Top = 50
      Width = 240
      Height = 25
      DataSource = dsMainTable
      TabOrder = 5
    end
    object cbKeyFloat: TCheckBox
      Left = 264
      Top = 58
      Width = 185
      Height = 17
      Caption = 'Show only Key + Float Fields'
      TabOrder = 6
      OnClick = cbKeyFloatClick
    end
    object SpinMagnitude: TSpinEdit
      Left = 508
      Top = 59
      Width = 92
      Height = 22
      MaxValue = 99999999
      MinValue = 0
      TabOrder = 7
      Value = 99999
    end
    object Button2: TButton
      Left = 672
      Top = 16
      Width = 75
      Height = 25
      Caption = 'Export'
      TabOrder = 8
      OnClick = Button2Click
    end
  end
  object MainGrid: TJvDBGrid
    Left = 0
    Top = 105
    Width = 631
    Height = 287
    Align = alClient
    DataSource = dsMainTable
    PopupMenu = PopupMenu1
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Visible = False
    OnDrawColumnCell = MainGridDrawColumnCell
    SelectColumnsDialogStrings.Caption = 'Select columns'
    SelectColumnsDialogStrings.OK = '&OK'
    SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
    EditControls = <>
    RowsHeight = 17
    TitleRowHeight = 17
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 392
    Width = 825
    Height = 19
    Panels = <
      item
        Width = 50
      end>
    ExplicitTop = 311
    ExplicitWidth = 635
  end
  object DBMemoCLOB: TDBMemo
    Left = 640
    Top = 105
    Width = 185
    Height = 287
    Align = alRight
    DataField = 'TransResponse'
    DataSource = dsMainTable
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Consolas'
    Font.Style = []
    ParentFont = False
    ScrollBars = ssBoth
    TabOrder = 3
    ExplicitLeft = 447
    ExplicitTop = 81
    ExplicitHeight = 230
  end
  object Database: TEDBDatabase
    Connected = True
    DatabaseName = 'Database'
    Database = 'NSiteData'
    SessionName = 'Session'
    Left = 48
    Top = 48
  end
  object Session: TEDBSession
    Connected = True
    LoginUser = 'Administrator '
    LoginPassword = 'EDBDefault'
    SessionName = 'Session'
    LocalSignature = 'edb_signature'
    LocalEncryptionPassword = 'elevatesoft'
    LocalLargeFileSupport = True
    LocalConfigPath = 'D:\POSData\QAData'
    LocalConfigName = 'EDBConfig'
    LocalConfigExtension = '.EDBCfg'
    LocalLockExtension = '.EDBLck'
    LocalLogExtension = '.EDBLog'
    LocalLogCategories = [lcInformation, lcWarning, lcError]
    LocalCatalogName = 'QAData'
    LocalCatalogExtension = '.EDBCat'
    LocalBackupExtension = '.EDBBkp'
    LocalUpdateExtension = '.EDBUpd'
    LocalTableExtension = '.EDBTbl'
    LocalTableIndexExtension = '.idx'
    LocalTableBlobExtension = '.blb'
    LocalTablePublishExtension = '.EDBPbl'
    LocalTempTablesPath = 'R:\Temp\'
    LocalCacheModules = False
    RemoteSignature = 'edb_signature'
    RemoteEncryption = True
    RemoteEncryptionPassword = 'elevatesoft'
    RemoteAddress = '127.0.0.1'
    Left = 48
    Top = 162
  end
  object EDBEngine: TEDBEngine
    Active = True
    Signature = 'edb_signature'
    EncryptionPassword = 'elevatesoft'
    LargeFileSupport = True
    LicensedSessions = 4096
    ConfigPath = 'D:\POSData\QAData'
    ConfigName = 'EDBConfig'
    ConfigExtension = '.EDBCfg'
    LockExtension = '.EDBLck'
    LogExtension = '.EDBLog'
    LogCategories = [lcInformation, lcWarning, lcError]
    CatalogName = 'EDBDatabase'
    CatalogExtension = '.EDBCat'
    BackupExtension = '.EDBBkp'
    UpdateExtension = '.EDBUpd'
    TableExtension = '.EDBTbl'
    TableIndexExtension = '.EDBIdx'
    TableBlobExtension = '.EDBBlb'
    TablePublishExtension = '.EDBPbl'
    TempTablesPath = 'R:\Temp\'
    CacheModules = False
    ServerName = 'EDBSrvr'
    ServerDescription = 'ElevateDB Server'
    ServerEncryptionPassword = 'elevatesoft'
    ServerAuthorizedAddresses.Strings = (
      '*')
    Left = 48
    Top = 104
  end
  object TableNamesQuery: TEDBQuery
    DatabaseName = 'Database'
    SessionName = 'Session'
    SQL.Strings = (
      'select Name from Information.Tables'
      'order by Name')
    Params = <>
    Left = 48
    Top = 224
    object TableNamesQueryName: TWideStringField
      FieldName = 'Name'
      Size = 40
    end
  end
  object TableFieldQuery: TEDBQuery
    DatabaseName = 'Database'
    SessionName = 'Session'
    SQL.Strings = (
      'select Name from Information.TableColumns'
      'where "TableName" = :tablename'
      'order by Name')
    Params = <
      item
        DataType = ftWideString
        Name = 'tablename'
        ParamType = ptInput
      end>
    Left = 128
    Top = 224
    object TableFieldQueryName: TWideStringField
      FieldName = 'Name'
      Size = 40
    end
  end
  object MainTable: TEDBTable
    AfterScroll = MainTableAfterScroll
    OnCalcFields = MainTableCalcFields
    DatabaseName = 'Database'
    SessionName = 'Session'
    TableName = 'MercuryTransLog'
    Left = 120
    Top = 48
    object MainTableTransactionID: TAutoIncField
      FieldName = 'TransactionID'
    end
    object MainTableTransRequet: TWideMemoField
      FieldName = 'TransRequet'
      Visible = False
      BlobType = ftWideMemo
    end
    object MainTableTransResponse: TWideMemoField
      FieldName = 'TransResponse'
      BlobType = ftWideMemo
    end
    object MainTableAmount: TFloatField
      FieldName = 'Amount'
    end
    object MainTableAuthorizationCode: TWideStringField
      FieldName = 'AuthorizationCode'
    end
    object MainTableApproved: TBooleanField
      FieldName = 'Approved'
    end
    object MainTableReferenceNumber: TWideStringField
      FieldName = 'ReferenceNumber'
    end
    object MainTableInvoiceNumber: TWideStringField
      FieldName = 'InvoiceNumber'
    end
    object MainTableProductName: TWideStringField
      FieldName = 'ProductName'
      Size = 50
    end
    object MainTableProductReceiptNumber: TWideStringField
      FieldName = 'ProductReceiptNumber'
      Size = 16
    end
    object MainTableCMDStatus: TWideStringField
      FieldName = 'CMDStatus'
    end
    object MainTableTextResponse: TWideStringField
      FieldName = 'TextResponse'
      Size = 50
    end
    object MainTableSequenceNumber: TWideStringField
      FieldName = 'SequenceNumber'
    end
    object MainTableEmployeeID: TIntegerField
      FieldName = 'EmployeeID'
    end
    object MainTableStationID: TIntegerField
      FieldName = 'StationID'
    end
    object MainTableCurrentDate: TDateField
      FieldName = 'CurrentDate'
    end
    object MainTableCurrentTime: TTimeField
      FieldName = 'CurrentTime'
    end
    object MainTableClientReceipt: TWideMemoField
      FieldName = 'ClientReceipt'
      BlobType = ftWideMemo
    end
    object MainTableMerchantReceipt: TWideMemoField
      FieldName = 'MerchantReceipt'
      BlobType = ftWideMemo
    end
    object MainTableUpdateCentralFlag: TIntegerField
      FieldName = 'Update Central Flag'
      Visible = False
    end
    object MainTableUpdateCentralDate: TDateField
      FieldName = 'Update Central Date'
      Visible = False
    end
    object MainTableUpdateCentralTime: TTimeField
      FieldName = 'Update Central Time'
      Visible = False
    end
    object MainTableCardType: TStringField
      DisplayWidth = 50
      FieldKind = fkCalculated
      FieldName = 'CardType'
      OnGetText = MainTableCardTypeGetText
      Size = 80
      Calculated = True
    end
    object MainTableCardApprovalInfo: TStringField
      DisplayWidth = 50
      FieldKind = fkCalculated
      FieldName = 'CardApprovalInfo'
      OnGetText = MainTableCardApprovalInfoGetText
      Size = 120
      Calculated = True
    end
  end
  object dsMainTable: TDataSource
    DataSet = MainTable
    Left = 288
    Top = 160
  end
  object PopupMenu1: TPopupMenu
    Left = 208
    Top = 136
    object ExportData1: TMenuItem
      Caption = 'Export Data'
    end
    object ExportAllByMonth1: TMenuItem
      Caption = 'Export All By Month'
    end
  end
  object JvCsvDataSet1: TJvCsvDataSet
    AutoBackupCount = 0
    Left = 416
    Top = 136
  end
  object XlsExporter: TJvDBGridExcelExport
    Caption = 'Export to Excel'
    Grid = MainGrid
    AutoFit = False
    Left = 672
    Top = 48
  end
  object csvExporter: TJvDBGridCSVExport
    Caption = 'Export as Comma Separated Variable (CSV) Text File'
    UseFieldGetText = True
    Grid = MainGrid
    ExportSeparator = esComma
    Left = 744
    Top = 64
  end
end
