object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Elevate Table Data Validation Tool'
  ClientHeight = 330
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 635
    Height = 81
    Align = alTop
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 16
      Width = 20
      Height = 13
      Caption = 'DBN'
    end
    object Label2: TLabel
      Left = 168
      Top = 16
      Width = 17
      Height = 13
      Caption = 'TBL'
    end
    object Label3: TLabel
      Left = 341
      Top = 16
      Width = 28
      Height = 13
      Caption = 'FIELD'
    end
    object Label4: TLabel
      Left = 508
      Top = 40
      Width = 92
      Height = 13
      Caption = 'Large Value Flag at'
    end
    object eDBN: TEdit
      Left = 34
      Top = 13
      Width = 63
      Height = 21
      TabOrder = 0
      Text = 'NSITEDATA'
    end
    object cbTableNames: TComboBox
      Left = 190
      Top = 13
      Width = 145
      Height = 21
      Style = csDropDownList
      Enabled = False
      ItemIndex = 0
      TabOrder = 1
      Text = 'Transaction File'
      OnChange = cbTableNamesChange
      Items.Strings = (
        'Transaction File'
        'Master'
        'Detail')
    end
    object cbFieldNames: TComboBox
      Left = 374
      Top = 13
      Width = 145
      Height = 21
      Style = csDropDownList
      Enabled = False
      TabOrder = 2
    end
    object Button1: TButton
      Left = 103
      Top = 11
      Width = 50
      Height = 25
      Caption = 'connect'
      TabOrder = 3
      OnClick = Button1Click
    end
    object eLocate: TEdit
      Left = 525
      Top = 13
      Width = 89
      Height = 21
      TabOrder = 4
      TextHint = 'Search for...'
      OnKeyPress = eLocateKeyPress
    end
    object DBNavigator1: TDBNavigator
      Left = 8
      Top = 50
      Width = 240
      Height = 25
      DataSource = dsMainTable
      TabOrder = 5
    end
    object cbKeyFloat: TCheckBox
      Left = 264
      Top = 58
      Width = 185
      Height = 17
      Caption = 'Show only Key + Float Fields'
      TabOrder = 6
      OnClick = cbKeyFloatClick
    end
    object SpinMagnitude: TSpinEdit
      Left = 508
      Top = 59
      Width = 92
      Height = 22
      MaxValue = 99999999
      MinValue = 0
      TabOrder = 7
      Value = 99999
    end
    object cbSafeMode: TCheckBox
      Left = 264
      Top = 40
      Width = 225
      Height = 17
      Caption = 'Safe Mode (leave checked if store is open)'
      Checked = True
      State = cbChecked
      TabOrder = 8
    end
  end
  object MainGrid: TJvDBGrid
    Left = 0
    Top = 81
    Width = 635
    Height = 230
    Align = alClient
    DataSource = dsMainTable
    PopupMenu = PopupMenu1
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Visible = False
    OnDrawColumnCell = MainGridDrawColumnCell
    SelectColumnsDialogStrings.Caption = 'Select columns'
    SelectColumnsDialogStrings.OK = '&OK'
    SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
    EditControls = <>
    RowsHeight = 17
    TitleRowHeight = 17
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 311
    Width = 635
    Height = 19
    Panels = <
      item
        Width = 50
      end>
    ExplicitLeft = 368
    ExplicitTop = 280
    ExplicitWidth = 0
  end
  object Database: TEDBDatabase
    DatabaseName = 'Database'
    Database = 'NSiteData'
    SessionName = 'Session'
    Left = 48
    Top = 48
  end
  object Session: TEDBSession
    Connected = True
    LoginUser = 'Administrator '
    LoginPassword = 'EDBDefault'
    SessionName = 'Session'
    LocalSignature = 'edb_signature'
    LocalEncryptionPassword = 'elevatesoft'
    LocalLargeFileSupport = True
    LocalConfigPath = 'D:\POSData\QAData'
    LocalConfigName = 'EDBConfig'
    LocalConfigExtension = '.EDBCfg'
    LocalLockExtension = '.EDBLck'
    LocalLogExtension = '.EDBLog'
    LocalLogCategories = [lcInformation, lcWarning, lcError]
    LocalCatalogName = 'QAData'
    LocalCatalogExtension = '.EDBCat'
    LocalBackupExtension = '.EDBBkp'
    LocalUpdateExtension = '.EDBUpd'
    LocalTableExtension = '.EDBTbl'
    LocalTableIndexExtension = '.idx'
    LocalTableBlobExtension = '.blb'
    LocalTablePublishExtension = '.EDBPbl'
    LocalTempTablesPath = 'R:\Temp\'
    LocalCacheModules = False
    RemoteSignature = 'edb_signature'
    RemoteEncryption = True
    RemoteEncryptionPassword = 'elevatesoft'
    RemoteAddress = '127.0.0.1'
    Left = 48
    Top = 162
  end
  object EDBEngine: TEDBEngine
    Active = True
    Signature = 'edb_signature'
    EncryptionPassword = 'elevatesoft'
    LargeFileSupport = True
    LicensedSessions = 4096
    ConfigPath = 'D:\POSData\QAData'
    ConfigName = 'EDBConfig'
    ConfigExtension = '.EDBCfg'
    LockExtension = '.EDBLck'
    LogExtension = '.EDBLog'
    LogCategories = [lcInformation, lcWarning, lcError]
    CatalogName = 'EDBDatabase'
    CatalogExtension = '.EDBCat'
    BackupExtension = '.EDBBkp'
    UpdateExtension = '.EDBUpd'
    TableExtension = '.EDBTbl'
    TableIndexExtension = '.EDBIdx'
    TableBlobExtension = '.EDBBlb'
    TablePublishExtension = '.EDBPbl'
    TempTablesPath = 'R:\Temp\'
    CacheModules = False
    ServerName = 'EDBSrvr'
    ServerDescription = 'ElevateDB Server'
    ServerEncryptionPassword = 'elevatesoft'
    ServerAuthorizedAddresses.Strings = (
      '*')
    Left = 48
    Top = 104
  end
  object TableNamesQuery: TEDBQuery
    DatabaseName = 'Database'
    SessionName = 'Session'
    SQL.Strings = (
      'select Name from Information.Tables'
      'order by Name')
    Params = <>
    Left = 48
    Top = 224
    object TableNamesQueryName: TWideStringField
      FieldName = 'Name'
      Size = 40
    end
  end
  object TableFieldQuery: TEDBQuery
    DatabaseName = 'Database'
    SessionName = 'Session'
    SQL.Strings = (
      'select Name from Information.TableColumns'
      'where "TableName" = :tablename'
      'order by Name')
    Params = <
      item
        DataType = ftWideString
        Name = 'tablename'
        ParamType = ptInput
      end>
    Left = 128
    Top = 224
    object TableFieldQueryName: TWideStringField
      FieldName = 'Name'
      Size = 40
    end
  end
  object MainTable: TEDBTable
    AfterScroll = MainTableAfterScroll
    DatabaseName = 'Database'
    SessionName = 'Session'
    Left = 120
    Top = 48
  end
  object dsMainTable: TDataSource
    DataSet = MainTable
    Left = 288
    Top = 160
  end
  object PopupMenu1: TPopupMenu
    Left = 208
    Top = 136
    object NaNFinder1: TMenuItem
      Caption = 'NaN Finder'
      OnClick = NaNFinder1Click
    end
    object RepairNanDamage1: TMenuItem
      Caption = 'Repair Nan Damage'
      OnClick = RepairNanDamage1Click
    end
  end
end
